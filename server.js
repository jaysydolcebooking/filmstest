const express = require ("express");
const bodyParser = require ("body-parser");
const pelis = require ("./moduloPelis");
const app = express();


app.use(express.static("public"));
app.use(bodyParser.urlencoded({ extended: true }))


app.use(bodyParser.json())


app.set("views", "./views"); 
app.set('view engine', 'pug'); 




app.get("/", pelis.getBuscador);

app.get("/api/films/:titulo", pelis.getapiFilms);

app.get("/films/:titulo", pelis.getpeliFinal);
 
app.get("/films/edit/:titulo", pelis.getPeliEditar);

app.get("/films/detalle/:titulo", pelis.getPeliDetalle);

app.get("/films/form/create/", pelis.getForm)

app.get("*", pelis.getError);




app.post("/films/create", pelis.posCreateFilms);

app.put("/films/edit", pelis.putEditarFilms)

app.delete("/films/delete", pelis.postDeleteFilms);


app.listen (3000, function ( ) { 
    console.log ('¡Test Application listening on port 3000!') 
  })
